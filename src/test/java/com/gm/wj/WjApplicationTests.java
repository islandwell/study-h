package com.gm.wj;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WjApplicationTests {

	@Test
	public void contextLoads() {
	}
	@Test
	public void test1() throws Exception {
		String username = "0922181242";
		String pattern = "^0922181[0-9]{2}$";
		if(!Pattern.matches(pattern,username)){
			throw new Exception("不匹配");
		}else
			System.out.println("匹配成功了");
	}

}
