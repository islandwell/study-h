package com.gm.wj.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "homework_user")
@JsonIgnoreProperties({ "handler","hibernateLazyInitializer" })
@Data
public class HomeWorkUser {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Integer studentId;
    private String username;
    private String password;
    private String name;
    @Column(name="class_job")
    private String classJob;
    private Integer status;
    private Integer su;
    @Column(name="create_time")
    private LocalDateTime createTime;
    @Column(name="is_deleted")
    private Boolean isDeleted;
}
