package com.gm.wj.dao;

import com.gm.wj.pojo.HomeWorkUser;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HomeWorkUserDao extends JpaRepository<HomeWorkUser,Integer> {
    HomeWorkUser findByUsername(String username);
    Integer countByUsername(String username);
}
