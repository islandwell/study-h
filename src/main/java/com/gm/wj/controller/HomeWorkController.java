package com.gm.wj.controller;

import com.alibaba.fastjson.JSONObject;
import com.gm.wj.result.Result;
import com.gm.wj.service.HomeWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyw
 * @date 2020/2/27 12:15
 */
@RequestMapping("/api/homework/common")
@RestController
public class HomeWorkController {
    @Autowired
    HomeWorkService homeWorkService;
    @PostMapping("/login")
    Result homeUserLogin(@RequestBody JSONObject jsonObject){
        return homeWorkService.homeUserLogin(jsonObject);
    }
    @PostMapping("/register")
    Result homeUserRegister(@RequestBody JSONObject jsonObject){
        return homeWorkService.homeUserRegister(jsonObject);
    }

}
