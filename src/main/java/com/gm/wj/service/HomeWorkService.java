package com.gm.wj.service;

import com.alibaba.fastjson.JSONObject;
import com.gm.wj.dao.HomeWorkUserDao;
import com.gm.wj.pojo.HomeWorkUser;
import com.gm.wj.result.Result;
import com.gm.wj.result.ResultCode;
import com.gm.wj.result.ResultFactory;
import com.gm.wj.util.JwtTokenutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Service
public class HomeWorkService {
    @Autowired
    HomeWorkUserDao homeWorkUserDao;
    public Result homeUserLogin(JSONObject jsonObject){
        String studentid = jsonObject.getString("studentid");
        String password = jsonObject.getString("password");
        if(StringUtils.isEmpty(studentid)||StringUtils.isEmpty(password)) {
            return ResultFactory.buildFailResult("账号或者密码为空");
        }
        //业务逻辑
        HomeWorkUser user = homeWorkUserDao.findByUsername(studentid);
        if(ObjectUtils.isEmpty(user)){
            return ResultFactory.buildFailResult("用户名不存在");
        }
        if(!DigestUtils.md5DigestAsHex(password.getBytes()).equals(user.getPassword())){
            return ResultFactory.buildFailResult("密码错误");
        }
        String token = null;
        try {
            token = JwtTokenutil.createToken(user.getUsername(),user.getStudentId(),user.getName(),user.getSu());
        }catch (Exception e){
            return ResultFactory.buildFailResult("密钥生成失败");
        }
        Map<String,Object> map = new HashMap<>();
        map.put("token",token);
        map.put("username",user.getUsername());
        map.put("name",user.getName());
        map.put("su",user.getSu());
        return ResultFactory.buildResult(ResultCode.SUCCESS,"成功",map);

    }
    public Result homeUserRegister(JSONObject jsonObject){
        String password1 = jsonObject.getString("password1");
        String password2 = jsonObject.getString("password2");
        String name = jsonObject.getString("name");
        String username = jsonObject.getString("username");
        String job = jsonObject.getString("job");
        if(StringUtils.isEmpty(password1)||StringUtils.isEmpty(password2)||StringUtils.isEmpty(name)||StringUtils.isEmpty(username)){
            return ResultFactory.buildFailResult("注册时表格尽量填写完整");
        }
        String pattern = "^0922181[0-9]{2}$";
        if(!Pattern.matches(pattern,username)){
            return ResultFactory.buildFailResult("对不起噢，本系统暂时只支持本班同学使用，请离开噢!");
        }
        if(!password1.equals(password2)){
            return ResultFactory.buildFailResult("太粗心啦，密码都输错了");
        }
        Integer count = homeWorkUserDao.countByUsername(username);
        if(count != 0){
            return ResultFactory.buildFailResult("想要强注别人的账号是不文明的行为，要注意噢！");
        }
        // 正常的注册业务逻辑
        HomeWorkUser homeWorkUser = new HomeWorkUser();
        password1 = DigestUtils.md5DigestAsHex(password1.getBytes());
        homeWorkUser.setPassword(password1);
        homeWorkUser.setName(name);
        homeWorkUser.setUsername(username);
        homeWorkUser.setClassJob(job);
        homeWorkUser.setStatus(1);
        homeWorkUser.setSu(0);
        homeWorkUser.setCreateTime(LocalDateTime.now());
        homeWorkUser.setIsDeleted(false);
        homeWorkUserDao.save(homeWorkUser);
        return ResultFactory.buildSuccessResult(null);
    }
}
